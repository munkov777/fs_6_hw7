import Enums.Species;

import java.util.Set;

public class Cat extends Pet implements Foul {
    Species species = Species.CAT;
    public Cat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Cat(String nickname) {
        super(nickname);
    }

    public Cat() {
    }

    public Cat(String nickname, Species species) {
        super(nickname);
        this.species = species;

    }

    @Override
    public void respond() {

    }
}
