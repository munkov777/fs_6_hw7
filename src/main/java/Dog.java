import Enums.Species;

import java.util.Set;

public class Dog extends Pet implements Foul{

    Species species = Species.DOG;




    public Dog(String nickname, int age, int trickLevel, Set<String> habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    }

    public Dog(String nickname, Species species) {
        super(nickname);
        this.species = species;
    }

    public Dog(Species species) {
        this.species = species;
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + super.getNickname() + "." + " Я соскучился!");
    }

   @Override
   public void foul() {
       System.out.println("Потрібно добре замести сліди...");

   }
}


