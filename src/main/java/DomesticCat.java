import Enums.Species;

import java.util.Set;

public class DomesticCat extends Pet implements Foul{
    Species species = Species.CAT;

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    }

    public DomesticCat(String nickname, Species species) {
        super(nickname);
        this.species = species;
    }

    public DomesticCat(Species species) {
        this.species = species;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + "." + " Я соскучился!");

    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");

    }

}
