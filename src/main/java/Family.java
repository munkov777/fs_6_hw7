import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<Human>();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human>  getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pet +
                '}';
    }

    //    @Override
//    protected void finalize(){
//        System.out.println("Object Family deleted");
//    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public void addChild(Human human) {
     children.add(human);
    }

    public boolean deleteChild(int index) {
        if (children.size() == 0) {
            System.out.println("Incorrect index");
            return false;
        } else if (index > children.size() | index < 0) {
            System.out.println("Incorrect index");
            return false;
        } else {
           children.remove(index);
            return true;
        }
    }

    public void deleteChild(Human human) {
        int index = 0;
        for (Human child : children) {
            if (child.getName().equals(human.getName())) {
                index++;
            }
        }
        if (index == 0) {
            System.out.println("No child with this name in this family");
        }
        if (children.size() == 0) {
            System.out.println("There is no child in this family");
        } else {
            for (int i = 0; i < children.size(); i++) {
                if ((children.get(i).hashCode() == human.hashCode()) && (children.get(i).getName().equals(human.getName()))) {
                    children.remove(i);

                }
            }
        }
    }

    public int countFamily() {
        return 2 + children.size();
    }
}