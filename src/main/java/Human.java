import Enums.DayOfWeek;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public abstract class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private HashMap<DayOfWeek, String> schedule;
    private Family family;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq,  HashMap<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human() {
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public  HashMap<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule( HashMap<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                ", family=" + family +
                '}';
    }

        @Override
    protected void finalize(){
    System.out.println("Object Human deleted");
   }

    public void greetPet() {
        System.out.println("Привет, " + family.getPet().getNickname() + "!");
    }

    public void describePet() {
        String trickLevel;
        if (family.getPet().getTrickLevel() > 50) {
            trickLevel = "очень хитрый";
            System.out.println( "У меня есть " + family.getPet().getSpecies() + ", ему " + family.getPet().getAge() + " лет, он " + trickLevel +".");
        } else {
            trickLevel = "почти не хитрый";
            System.out.println( "У меня есть " + family.getPet().getSpecies() + ", ему " + family.getPet().getAge() + " лет, он " + trickLevel + ".");
        }
    }

    public void feedPet(boolean timeToEat) {
        Random random = new Random();
        int temp = random.nextInt(100);
        if (timeToEat) {
            System.out.println("Хм... покормлю ка я " + family.getPet().getNickname());
        }
        if (family.getPet().getTrickLevel() > temp) {
            System.out.println("Хм... покормлю ка я " + family.getPet().getNickname());
        } else {
            System.out.println("Думаю, " + family.getPet().getNickname() + " не голоден.");
        }
    }


}
