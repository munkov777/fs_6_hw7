import Enums.DayOfWeek;
import Enums.Species;

import java.util.HashMap;
import java.util.Map;


public class Main {
    public static void main(String[] args) {
        //first family
        Man viktor = new Man("Viktor", "Munko", 40);
        viktor.setIq(150);


        HashMap<DayOfWeek, String> scheduleViktor = new HashMap<DayOfWeek, String>();
        scheduleViktor.put(DayOfWeek.SATURDAY, "Play football");
        scheduleViktor.put(DayOfWeek.SUNDAY, "Goto the gym");

        Woman halyna = new Woman("Halyna", "Munko", 38);
        halyna.setIq(150);
        HashMap<DayOfWeek, String> scheduleHalyna = new HashMap<DayOfWeek, String>();
        scheduleHalyna.put(DayOfWeek.SATURDAY, "Go shopping");
        scheduleHalyna.put(DayOfWeek.SUNDAY, "Play tennis");
        Family familyWick = new Family(halyna, viktor);
        Dog rej = new Dog("Rej", Species.DOG);
        familyWick.setPet(rej);
        Cat cat = new Cat("Lulu", Species.CAT);
        familyWick.setPet(cat);
        Man tymur = new Man("Tymur", "Munko", 1990, 97, scheduleViktor);
        familyWick.addChild(tymur);
        //tymur.setFamily(familyWick);
        Man bohdan = new Man("Bohdan", "Munko", 2008, 109, scheduleViktor);
        familyWick.addChild(bohdan);
        //bohdan.setFamily(familyWick);

        System.out.println(familyWick);
    }
}
