import Enums.Species;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;


public class PetTest {


    Dog rej = new Dog("Rej", Species.DOG);
    Cat cat = new Cat("Lulu", Species.CAT);

    @Test
    void testHashcode(){
        assertTrue(rej.hashCode()!=cat.hashCode());
    }




    @Test
    void getNickname() {
        String expected = String.valueOf(rej.getNickname());
        String actual = rej.getNickname();
        Assert.assertEquals(expected, actual);

    }
    @Test
    void testEqualsFalse(){
        assertFalse(rej.equals(cat));
    }


}
